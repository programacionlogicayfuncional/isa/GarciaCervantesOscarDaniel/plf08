# plf08

## Descripción del proyecto.

Este proyecto sirve para cifrar por el metodo del cuadrado de polibio
y la transformación lineal un archivo, en el primero se recibe solo el texto y por
medio de piezas de ajedrez se imprime el resultado en otro archivo.

En el segundo se recibe una palabra clave de longitud mayor a 7 en el cual 
por medio de un archivo lo lee y lo saca de igual manera.

## Forma de ejecutar.

Desde la consola se escribe > lein run metodo op args

## Opciones para su ejecución.

metodo es "polibio" o "tcolumnar"
op en polibio y tcolumnar es "codificar" o "decodificar"
args en polibio "ruta del archivo a leer" "ruta del archivo a escribir"
args en tcolumnar es "palabra clave" "ruta del archivo a leer" "ruta del archivo a escribir"

## Ejemplos de uso o ejecución.

lein run "tcolumnar" "codificar" "abcdefghl" "C:/Users/Seryve/plf08/resources/Texto/kafka.txt" "C:/Users/Seryve/plf08/resources/Texto/lcifrado.txt"

lein run "polibio" "codificar"  "C:/Users/Seryve/plf08/resources/Texto/kafka.txt" "C:/Users/Seryve/plf08/resources/Texto/pcifrado.txt" " "

lein run "tcolumnar" "decodificar" "abcdefghl" "C:/Users/Seryve/plf08/resources/Texto/lcifrado.txt" "C:/Users/Seryve/plf08/resources/Texto/ldecifrado.txt"

lein run "polibio" "decodificar"  "C:/Users/Seryve/plf08/resources/Texto/pcifrado.txt" "C:/Users/Seryve/plf08/resources/Texto/pdecifrado.txt" " "

## Errores, limitantes, situaciones no consideradas o particularidades de tu solución.

En el polibio hay un error de dos signos que no estan en el mapa, ¡¿ por lo que al momento de decifrar los saca como inversos en el de transformacion columnar al momento de repetir una letra en la palabra clave no las toma en cuenta por lo que esa implementacion a cpalabra falto, y como se respetan los caracteres que no estan dentro del alfabeto se completan con espacios para que los vectores dentro de los mapas sean del mismo tamaño y pueda dar solucion al cifrado y decifrado.

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar plf08-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.

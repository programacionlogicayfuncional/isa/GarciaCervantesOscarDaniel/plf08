(ns plf08.core
  (:gen-class)
  (:require [plf08.transformacion-columnar :as tcolum]
            [plf08.cpolibio :as poli]))

(defn -main
  [metodo op arg1 arg2 arg3]
  (if (and (empty? metodo) (or (not= metodo "tcolumnar") (not= metodo "polibio")))
    (println "Error, no se ha seleccionado ningun metodo a cifrar")
    (if (= metodo "tcolumnar")
      (tcolum/-main op arg1 arg2 arg3)
      (poli/-main op arg1 arg2))))

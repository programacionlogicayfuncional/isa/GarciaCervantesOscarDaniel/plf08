(ns plf08.transformacion-columnar
  (:require [clojure.string :as str]))

(defn alfabetol
  []
  (zipmap [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z \0 \1 \2 \3 \4 \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9]
          [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 58 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75]))

(defn cpalabra
  [r]
  (let [s (str/lower-case r)
        f (into [] s)]
    (replace (alfabetol) f)))

(defn lcodificar
  [r t]
  (let [c (str/lower-case r)
        p  (str/split (apply str t (repeat (- (count c) (mod (count t) (count c))) " ")) #"")
        s (apply mapv vector (partition-all (count c) p))]
    (apply str (flatten (vals (sort (zipmap (cpalabra c) s)))))))

(defn ldecodificar
  [r t]
  (let [c (str/lower-case r)
        p (apply mapv vector (partition (/ (count t) (count c)) (str/split t #"")))
        z (zipmap (vec (sort (cpalabra c))) (apply mapv vector p))]
    (apply str (flatten (apply mapv vector (replace z (cpalabra c)))))))

(defn -main
  [op arg1 arg2 arg3]
  (if (empty? op)
    (println "Error, no se ha dado algún texto a cifrar.")
    (if (and (= op "codificar") (> (count arg1) 7))
      (spit arg3 (lcodificar arg1 (slurp arg2)))
      (if (and (= op "decodificar") (> (count arg1) 7))
        (spit arg3 (ldecodificar arg1 (slurp arg2)))
        (println "La palabra clave debe ser mayor a 8 y debes introducir todos los argumentos")))))
(ns plf08.core-test
  (:require [clojure.test :refer :all]
            [plf08.core :refer :all]
            [plf08.transformacion-columnar :as tcolum]
            [plf08.cpolibio :as poli]))

(deftest cpalabra-test
  (testing "Palabras clave"
    (is (= [4 1 16 6 23 11 17 19 24] (tcolum/cpalabra "camerinos")))
    (is (= [1 3 4 5 6] (tcolum/cpalabra "ABCDE")))
    (is (= [25 23 1 17 24 16 11 15 6] (tcolum/cpalabra "transmile")))
    (is (= [1 3 4 5 35 36 37 8 9 10 11 13 14 15 16 17 19 21 22 23 24 25 26 29 31 32 33 50 53] (tcolum/cpalabra "ABCD123fghijklmnopqrSTuvxyz,/")))))

(deftest lcodificar-test
  (testing "codificar con la palabra clave" 
    (is (= "h ts lkeeby   sui " (tcolum/lcodificar "camerinos" "the sky is blue")))
    (is (= "te h gear nesdek nyo  c ie sa  n b  li us " (tcolum/lcodificar "abcdefghijklmn" "the sky is blue and ocean is green")))
    (is (= "a1s2d5a n7 6f d s f   s d f w e 1 2 3 1 2   3 1 s3d6j82 3 " (tcolum/lcodificar "abcd123fghijklmnopqrstuvxyz,/" "asdasdjn fdsf sdfwe12312 3123125 36876")))))

(deftest ldecodificar-test
  (testing "decodificar con la palabra clave"
    (is (= "the sky is blue   " (tcolum/ldecodificar "camerinos" "h ts lkeeby   sui ")))
    (is (= "the sky is blue and ocean is green        " (tcolum/ldecodificar "abcdefghijklmn" "te h gear nesdek nyo  c ie sa  n b  li us ")))
    (is (= "asdasdjn fdsf sdfwe12312 3123125 36876       " (tcolum/ldecodificar "transmile" "ds21  w18 jd 3 nf36 ds2  af32 sd136s 15 afe27")))
    (is (= "asdasdjn fdsf sdfwe12312 3123125 36876                    " (tcolum/ldecodificar "abcd123fghijklmnopqrstuvxyz,/" "a1s2d5a n7 6f d s f   s d f w e 1 2 3 1 2   3 1 s3d6j82 3 ")))))

(deftest cifrar-test
  (testing "Cifrado de 1 sola letra"
    (is (= "♞♞" (poli/pcodificar "í")))
    (is (= "♚♕" (poli/pcodificar "+")))
    (is (= "♗♜" (poli/pcodificar "5")))
    (is (= "♔♜" (poli/pcodificar "Q")))
    (is (= "♝♗" (poli/pcodificar "ü")))
    (is (= "♚♛" (poli/pcodificar "&"))))
  (testing "Cifrado de palabras"
    (is (= "♜♔♞♕♞♚♜♜" (poli/pcodificar "hola")))
    (is (= "♗♖♔♔♔♚♕♖" (poli/pcodificar "AZUL")))
    (is (= "♞♖♞♕♝♝♜♜♜♚♞♕" (poli/pcodificar "morado")))
  (testing "Cifrado de cadenas"
    (is (= "♕♘♜♖ ♜♕♝♖♝♛♝♚♜♜ ♞♚♜♜ ♞♚♞♚♝♖♝♕♞♜♜♜" (poli/pcodificar "Me gusta la lluvia")))
    (is (= "♕♘♞♜ ♜♛♞♕♞♚♞♕♝♝ ♜♗♜♜♝♕♞♕♝♝♞♜♝♚♞♕ ♜♖♝♛ ♜♖♞♚ ♜♜♞♖♜♜♝♝♞♜♞♚♞♚♞♕" (poli/pcodificar "Mi color favorito es el amarillo"))))))

(deftest descifrar-test
  (testing "Descifrado de 1 sola letra"
    (is (= "í" (poli/pdecodificar "♞♞")))
    (is (= "+" (poli/pdecodificar "♚♕")))
    (is (= "5" (poli/pdecodificar "♗♜")))
    (is (= "Q" (poli/pdecodificar "♔♜")))
    (is (= "ü" (poli/pdecodificar "♝♗")))
    (is (= "&" (poli/pdecodificar "♚♛"))))
  (testing "Cifrado de palabras"
    (is (= "hola" (poli/pdecodificar "♜♔♞♕♞♚♜♜")))
    (is (= "AZUL" (poli/pdecodificar "♗♖♔♔♔♚♕♖")))
    (is (= "Amarillo" (poli/pdecodificar "♗♖♞♖♜♜♝♝♞♜♞♚♞♚♞♕")))
    (is (= "morado" (poli/pdecodificar "♞♖♞♕♝♝♜♜♜♚♞♕")))
  (testing "Cifrado de cadenas"
    (is (= "Me gusta la lluvia" (poli/pdecodificar "♕♘♜♖ ♜♕♝♖♝♛♝♚♜♜ ♞♚♜♜ ♞♚♞♚♝♖♝♕♞♜♜♜")))
    (is (= "Mi color favorito es el amarillo" (poli/pdecodificar "♕♘♞♜ ♜♛♞♕♞♚♞♕♝♝ ♜♗♜♜♝♕♞♕♝♝♞♜♝♚♞♕ ♜♖♝♛ ♜♖♞♚ ♜♜♞♖♜♜♝♝♞♜♞♚♞♚♞♕"))))))